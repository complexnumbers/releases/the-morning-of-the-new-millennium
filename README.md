# Build files for "The Morning of the New Millennium" album

Index file: https://complexnumbers.gitlab.io/releases/the-morning-of-the-new-millennium/

## License
### The Morning of the New Millennium (c) by Victor Argonov Project

The Morning of the New Millennium is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
